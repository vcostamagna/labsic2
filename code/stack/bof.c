
#define BUF_SIZE 64

void bof(char** argv){
    char buf[BUF_SIZE];
    printf("buf addr: %p \n", &buf);
    strcpy(&buf, argv[1]);
}


void main(int argc, char**argv)
{
	bof(argv);
}
