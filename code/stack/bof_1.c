
#define BUF_SIZE 64
#define MAGIC "/bin/sh"

int logging(char* c){/*logging code*/ return 1;}
void secret(){system(MAGIC);}

struct Person{
    char name[BUF_SIZE];
    int (*desc)(char*);
};
typedef struct Person person;

void bof(char** argv, person* p){
    printf("got person @ %p\n", p);
    while(1){
        strcpy(p->name, argv[1]);
        printf("person name: %s \n", p->name);
        break;
    }
    
}

void aperson(char** argv){
    person p;
    p.desc = logging;
    bof(argv, &p);
    p.desc(p.name);
}

void main(int argc, char**argv)
{
    aperson(argv);
}
