
#define BUF_SIZE 64
#define MAGIC "/bin/sh"

int logging(char* c){/*logging code*/ return 1;}
//void secret(){system(MAGIC);}

struct Person{
    char name[BUF_SIZE];
    int (*desc)(char*);
};
typedef struct Person person;

void bof(char** argv, person* p, int flag){
    printf("got person @ %p\n", p);
    while(1){
        if(flag)
            strcpy(p->name, argv[1]);
        else
            strcpy(p->name, argv[2]);
        printf("person name: %s \n", p->name);
        break;
    }
    
}

void aperson(char** argv){
    person p, p2;
    p.desc = logging;
    bof(argv, &p,1);
    bof(argv, &p2,0);
    printf("p2 name: %s\n", p2.name);
    p.desc(p2.name);
}

void main(int argc, char**argv)
{
    aperson(argv);
}
