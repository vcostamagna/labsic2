#!/bin/bash

if [ ! -d "all" ]; then
    mkdir -p "all"
fi

make -f Makefile.all clean
make -f Makefile.all
