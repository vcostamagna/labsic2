#include <errno.h>   // for errno
#include <limits.h>  // for INT_MAX
#include <stdlib.h>  // for strtol
#define BASE 10
#define SIZE 256 

void hello(){ printf("hello world \n"); }
/*
void shell(){ 
	setreuid(0);
	seteuid(0);
	system("/bin/bash"); }
*/
void write_what_where(char **argv)
{
	char buf[SIZE];
	char* pbuf = &buf;
	char* stop;
	//unsigned long* where = (unsigned long*) strtoul(argv[1], &stop, BASE);
	unsigned long where = (unsigned long) strtoul(argv[1], &stop, BASE);
	unsigned long what = (unsigned long*) strtoul(argv[2], &stop, BASE);
	//printf("what: %lu, where: %lu, pbuf @ %p \n", what, where, pbuf);
	unsigned long* wherep = where;
	*wherep = what;
	//memcpy(pbuf+where, &what, 4);
	//printf("scritto %ul @ %p\n", what, where); 
	printf("/bin/sh"); // i am lazy :)
}
void altered(char ** argv)
{
	char* stop;
	int num;
	errno = 0;
	unsigned long l = strtoul(argv[1], &stop, BASE);
	unsigned long *lp = l;
	void (*fp)();
	fp = hello;
	fp();
	lp = &fp;
	printf("scrivo in %p : %s \n", lp, argv[2]);
	memcpy(lp, argv[2], 4);
	//*lp = 0x41414141;
	fp();
}

void normal(char ** argv)
{
	char* stop;
	int num;
	errno = 0;
	unsigned long l = strtoul(argv[1], &stop, BASE);
	if (errno != 0 || *stop != '\0') {
	    // Put here the handling of the error, like exiting the program with
	    // an error message
	} else {
	    // No error
	    //num = l;    
	    printf("letto: %lu \n",l);
	}
	void (*fp)();
	fp = hello;
	fp();
}

int main(int argc, char** argv)
{
	//shell();
	write_what_where(argv);

	return 0;
}
