#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define BUF_SIZE 256
#define NAME_LEN 124
#define MAX 50

struct Person
{
    void* desc;
    char name[NAME_LEN];
};

typedef struct Person person;




static person* bag[MAX];
static int counter = 0;

void _read(void* buf,int size);
/*
void shell(){ 
	setreuid(0);
	seteuid(0);
	system("/bin/bash"); }
*/


void int_overflow()
{
    char* p;
    char buf[BUF_SIZE/2];
    int array[BUF_SIZE];
    char* c;
    int index;
    unsigned long what;
    p = buf;
    printf("index: ");
    scanf("%u%c", &index, &c);
    if(index > BUF_SIZE)
    {
        printf("out of bound index\n");
        return;
    }
    printf("what: ");
    scanf("%lu%c", &what, &c);
    array[index] = what;
    printf("read into buf: ");
    _read(p,BUF_SIZE/2);
}

void write_what_where()
{
	char buf[BUF_SIZE];
	char* stop;
	unsigned long where, what;
	printf("where: ");
	scanf("%lu%c", &where, &stop);
	printf("what: ");
	scanf("%lu%c", &what, &stop);
	//printf("what: %lu, where: %lu, pbuf @ %p \n", what, where, pbuf);
	unsigned long* wherep = (unsigned long*) where;
	*wherep = what;
	//printf("scritto %ul @ %p\n", what, where); 
	printf("/bin/sh"); // i am lazy :)
}


void _read(void* buf,int size)
{
    char* c;
    fgets(buf, size, stdin);
    if ((c=strchr(buf, '\n')) != NULL)
        *c = '\0';
    //printf("_read: %s len: %d \n", buf, strlen(buf));
}
void print_person(int index)
{
    if(index >= counter) return;
    person* p = bag[index];
    if(!p) return;
    printf("name: %s\n", p->name);
    printf("desc: %s\n", p->desc); 
}

void del(int idx)
{
    if(idx >= counter) return;
    person* p = bag[idx];
    if(!p) return;
    free(p->desc);
    free(p);
    bag[idx] = 0x0;
}
void edit_desc(int idx)
{
    int size;
    char c;
    unsigned int t;
    if(idx >= counter) return;
    person* p = bag[idx];
    if(!p) return;
    printf("length: ");
    scanf("%u%c", &size, &c);
    t = (unsigned int) p->desc + size;
    unsigned int limit = (unsigned int)p - 4;
    //printf("starting from: %x to: %x chunk @%x \n", p->desc, t, limit);
    if( t >= limit)
    {
        printf("you cannot fool this protection!!\n");
        exit(1);
    }
    printf("text: ");
    _read(p->desc, size + 1);
}
void add_person(int desc_size)
{
    int size;
    char* c;
    char* desc;
    if(desc_size)
        desc = malloc(sizeof(char)*desc_size);
    person* p = malloc(sizeof(struct Person));
    p->desc = desc;
    //printf("person: @%p \n", p);
    printf("write name: ");
    _read(p->name, NAME_LEN + 1);
    bag[counter] = p;
    counter += 1;
    edit_desc(counter - 1);
}

void heap()
{
    char c, p;
    int index, size, i;
    while(1)
    {
        printf("\n==================== HEAP =======================\n");
        printf("0 - add person\n");
        printf("1 - print person\n");
        printf("2 - edit person \n");
        printf("3 - del person \n");
        printf("4 - Exit\n");
        printf("Action: ");
        scanf("%d", &i);
        switch(i)
        {       
            case 0:
                printf("len description: ");
                scanf("%u%c", &size, &p);
                //printf("len description: %d\n", size);
                add_person(size);
                break;
            case 1:
                printf("index: ");
                scanf("%u%c", &index, &p);
                print_person(index);
                break;
            case 2:
                printf("index: ");
                scanf("%u%c", &index, &p);
                edit_desc(index);
                break;
            case 3:
                printf("index: ");
                scanf("%u%c", &index, &p);
                del(index);
                break;
            case 4:
                return;
            default:
                break;
        }       
        if(counter > MAX - 1)
        {
            printf("bag is full. bye!\n");
            break;
        }
    }
}

struct _bof
{
    char name[BUF_SIZE];
    int (*f_ptr)(char*); //function pointer
};
typedef struct _bof abof;

void bof(char** argv){
    char buf[BUF_SIZE];
    int size;
    char c;
    if(!argv[1] || !argv[2]) return;
    abof* b = (abof*) malloc(sizeof(struct _bof));
    printf("b->name @%p \n", b->name);
    printf("data: ");
    strcpy(b->name, argv[1]);
    strcpy(buf, argv[2]);
    b->f_ptr(buf);
    printf("pointer value: %p\n", b->f_ptr);
}

void infinite_loop(char** argv)
{
    char c, p;
    int i;
	while(1)
    {
        printf("\n--------------------------- CTF ----------------------------- \n");
        printf("0 - BOF\n");
        printf("1 - write-what-where\n");
        printf("2 - heap \n");
        printf("3 - int overflow\n");
        printf("4 - Exit\n");
        printf("Action: ");
        scanf("%d", &i);
        switch(i)
        {       
            case 0:
                bof(argv);
                break;
            case 1:
                write_what_where();
                break;
            case 2:
                heap();
                break;
            case 3:
                int_overflow();
                break;
            case 4:
                exit(1);
                break;
            default:
                break;
        }        
	}
}

int main(int argc, char**argv)
{
    setvbuf(stdin, 0, 0x2, 0);
    setvbuf(stdout, 0, 0x2, 0);
    infinite_loop(argv);
}
